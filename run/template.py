import sys
sys.path.append('../')
import applications.deterministics as det
import applications.heuritics as heu
import bin.inputManipulation as im
import bin.plots as plt

fileName = "./var.dat"
listaVar = [3, 2, 50, 50]

im.writeNewInput(fileName, listaVar)
#varListIter, functionList, nObjFuncEval = det.steepestDescent(0.01,fileName,"forward",1e-8,1000,1e-8,True)
#varListIter, functionList, nObjFuncEval = det.user_conjugateGradient(0.01,fileName,"forward",1e-8,1000,1e-8,True)
#varListIter, functionList, nObjFuncEval = det.conjugateGradient(0.01,fileName,"forward",1e-8,20,1e-8,True)
#varListIter, functionList, nObjFuncEval = det.user_newton(fileName,"forward",1e-1,100,1e-8,True)
#varListIter, functionList, nObjFuncEval = det.newton(fileName,"forward",1e-1,100,1e-8,True)
#varListIter, functionList, nObjFuncEval = det.user_BFGS(0.01,fileName,"forward",1e-8,1000,1e-8,True)
#varListIter, functionList, nObjFuncEval = det.BFGS(0.01,fileName,"forward",1e-8,1000,1e-8,True)

bounds=[(-100, 100), (-100, 100)]
#varListIter, functionList, nObjFuncEval = heu.differentialEvolution(bounds,fileName,12,1000,1e-2,True)
varListIter, functionList, nObjFuncEval = heu.particleSwarm(bounds,fileName,100,20,1e-2,True)
#varListIter, functionList, nObjFuncEval = heu.simulatedAnnealing(bounds, fileName, 300, 1e-5, 1e-2, 1000, True)

# print(len(varListIter))

# print(len(functionList))

# print(len(nObjFuncEval))

# print(len(varListIter))

# plots
plt.funcConvergence3D(varListIter, functionList, 'Function 3, sp = [50,50], PS method')
plt.funcByNEval(nObjFuncEval, functionList,'Function 3, sp = [50,50], PS method',3)

'''
import multiprocessing

code multirocesssing

    n_CPU = multiprocessing.cpu_count()
    runDir = os.getcwd()
    dirsList = []
    for cpu in range(n_CPU):
        os.mkdir(f"{runDir}/processor{cpu}")
        dirsList.append(f"{runDir}/processor{cpu}")
'''
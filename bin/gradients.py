import bin.finiteDifference as fd
import bin.objectiveFunction as of
import bin.inputManipulation as im
import os
import numba as nb
import numpy as np

def grad_central(inputPath: str, delta: float):
    """Central approximation gradient.
    Calculates finite difference central approximation gradient for each input variable based on a given local pertubation.
    The pertubation is calculated relatively to the local value.

    Args:\n
        inputPath (str): path to the input file with the variables\n
        delta (float): value of the relative local pertubation\n
        
    Returns:\n
        grads (list): list with the calculated gradients for each variable\n   
    """
    varList = im.readInput(inputPath)
    dx_pos = []
    dx_neg = []
    relativeDeltas = []
    i = 0
    while i < varList[1]:
        dx_pos.append(varList[:])
        relativeDelta = delta*dx_pos[-1][i+2]
        dx_pos[-1][i+2] += relativeDelta 
        dx_neg.append(varList[:])
        dx_neg[-1][i+2] -= relativeDelta
        relativeDeltas.append(relativeDelta)
        i+=1
    Ux_pos = []
    Ux_neg = []
    for input in dx_pos:
        im.writeNewInput(inputPath, input)
        Ux_pos.append(of.runDir(inputPath))
    for input in dx_neg:
        im.writeNewInput(inputPath, input)
        Ux_neg.append(of.runDir(inputPath))
    #print(Ux_pos,'\n',Ux_neg)
    grads = []
    i = 0
    while i < varList[1]:
        grads.append(fd.central(Ux_neg[i], Ux_pos[i], relativeDeltas[i]))
        i+=1
    return grads

def grad_forward(inputPath: str, delta: float):
    """Forward approximation gradient.
    Calculates finite difference forward approximation gradient for each input variable based on a given local pertubation.
    The pertubation is calculated relatively to the local value.

    Args:\n
        inputPath (str): path to the input file with the variables\n
        delta (float): value of the relative local pertubation\n
        
    Returns:\n
        grads (list): list with the calculated gradients for each variable\n   
    """
    varList = im.readInput(inputPath)
    dx_pos = []
    of.runDir(inputPath)
    objPath = os.getcwd() + "/functionsModule/obj.dat"
    Ux = im.readOutput(objPath)
    i = 0
    relativeDeltas = []
    while i < varList[1]:
        dx_pos.append(varList[:])
        relativeDelta = delta*dx_pos[-1][i+2]
        dx_pos[-1][i+2] += relativeDelta
        relativeDeltas.append(relativeDelta)
        i+=1
    #print(dx_pos)
    Ux_pos = []
    for input in dx_pos:
        im.writeNewInput(inputPath, input)
        Ux_pos.append(of.runDir(inputPath))
        #print('rodei')
    grads = []
    i = 0
    while i < varList[1]:
        grads.append(fd.forward(Ux, Ux_pos[i], relativeDeltas[i]))
        i+=1
    return grads

def grad_backward(inputPath: str, delta: float):
    """Backward approximation gradient.
    Calculates finite difference backward approximation gradient for each input variable based on a given local pertubation.
    The pertubation is calculated relatively to the local value.

    Args:\n
        inputPath (str): path to the input file with the variables\n
        delta (float): value of the relative local pertubation\n
        
    Returns:\n
        grads (list): list with the calculated gradients for each variable\n   
    """
    varList = im.readInput(inputPath)
    dx_neg = []
    of.runDir(inputPath)
    objPath = os.getcwd() + "/functionsModule/obj.dat"
    Ux = im.readOutput(objPath)
    i = 0
    relativeDeltas = []
    while i < varList[1]:
        dx_neg.append(varList[:])
        relativeDelta = delta*dx_neg[-1][i+2]
        dx_neg[-1][i+2] -= relativeDelta
        relativeDeltas.append(relativeDelta)
        i+=1
    Ux_neg = []
    for input in dx_neg:
        im.writeNewInput(inputPath, input)
        Ux_neg.append(of.runDir(inputPath))
    grads = []
    i = 0
    while i < varList[1]:
        grads.append(-fd.backward(Ux, Ux_neg[i], relativeDeltas[i]))
        i+=1
    return grads

def second_derivative_central(inputPath: str, delta: float):
    """Central approximation second derivative.
    Calculates finite difference central approximation gradient for each input variable based on a given local pertubation.
    The pertubation is calculated relatively to the local value.

    Args:\n
        inputPath (str): path to the input file with the variables\n
        delta (float): value of the relative local pertubation\n
        
    Returns:\n
        grads (list): list with the calculated gradients for each variable\n   
    """
    varList = im.readInput(inputPath)
    dx_pos = []
    dx_neg = []
    relativeDeltas = []
    i = 0
    while i < varList[1]:
        dx_pos.append(varList[:])
        relativeDelta = delta*dx_pos[-1][i+2]
        dx_pos[-1][i+2] += relativeDelta 
        dx_neg.append(varList[:])
        dx_neg[-1][i+2] -= relativeDelta
        relativeDeltas.append(relativeDelta)
        i+=1

    U = of.runDir(inputPath)
    Ux_pos = []
    Ux_neg = []
    for input in dx_pos:
        im.writeNewInput(inputPath, input)
        Ux_pos.append(of.runDir(inputPath))
    for input in dx_neg:
        im.writeNewInput(inputPath, input)
        Ux_neg.append(of.runDir(inputPath))
    #print(Ux_pos,'\n',Ux_neg, 'n', U)
    grads = []
    i = 0
    while i < varList[1]:
        grads.append(fd.second_central(Ux_pos[i], Ux_neg[i], U, relativeDeltas[i]))
        #print(Ux_pos[i] - 2*U + Ux_neg[i])
        #print(relativeDeltas[i]**2)
        i+=1
    return grads

def second_cross_derivative(inputPath: str, delta: float):
    """Central approximation second cross derivative.
    Calculates finite difference central approximation cross gradient for each input variable based on a given local pertubation.
    The pertubation is calculated relatively to the local value.

    Args:\n
        inputPath (str): path to the input file with the variables\n
        delta (float): value of the relative local pertubation\n
        
    Returns:\n
        grads (list): list with the calculated cross gradients for each variable\n   
    """
    varList = im.readInput(inputPath)
    a = int(varList[1])
    A = np.zeros((a,a))
    #Us = []
    
    # for i in range(a):
    #     linha = []
    #     for j in range(a):
    #         linha.append(U_FF, U_FB, U_BF, U_BB)
    #     Us.append(linha)

    for i in nb.prange(a):
        while i < (a**2-a)/2:
            input1 = []
            input2 = []
            input3 = []
            input4 = []
            input1.extend(varList)
            input2.extend(varList)
            input3.extend(varList)
            input4.extend(varList)
            delta1 = delta*varList[i+2]
            delta2 = delta*varList[i+3]
            input1[i+2] += delta1*input1[i+2]
            input1[i+3] += delta2*input1[i+3]
            input2[i+2] += delta1*input2[i+2]
            input2[i+3] -= delta2*input2[i+3]
            input3[i+2] -= delta1*input3[i+2]
            input3[i+3] += delta2*input3[i+3]
            input4[i+2] -= delta1*input4[i+2]
            input4[i+3] -= delta2*input4[i+3]
            im.writeNewInput(inputPath, input1)
            U_FF = of.runDir(inputPath)
            im.writeNewInput(inputPath, input2)
            U_FB = of.runDir(inputPath)
            im.writeNewInput(inputPath, input3)
            U_BF = of.runDir(inputPath)
            im.writeNewInput(inputPath, input4)
            U_BB = of.runDir(inputPath)
            A[i,1+i:] = A[1+i:,i] = fd.cross_second(U_FF, U_FB, U_BF, U_BB, delta1, delta2)
            #print(f'A -> {A}')
            i+=1
            
    return A
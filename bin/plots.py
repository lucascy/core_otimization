import matplotlib.pyplot as plt

def funcConvergence3D(varListIter: list, functionList: list, name: str):
    plt.rcParams.update({'font.size': 16})
    plt.rcParams["font.family"] = "sans-serif"
    plt.rcParams["figure.figsize"] = [12, 12]
    plt.rcParams["figure.autolayout"] = True

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    x = []
    y = []
    for i in varListIter:
        x.append(i[0])
        y.append(i[1])

    ax.plot(x, y, functionList, 'r', linestyle="-.", linewidth=2, markersize=10, marker="x", markevery=1, label=name)
    ax.legend(fontsize=12)
    ax.set_xlabel('x1', fontsize=20, labelpad=13)
    ax.set_ylabel('x2', fontsize=20, labelpad=13)
    ax.set_zlabel('Function Value', fontsize=20, labelpad=15)
    plt.title(name)
    fig.savefig(f'funcConvergence3D_{name}.png', dpi=fig.dpi)

def funcByNEval(nObjFuncEval: list, functionList: list, name: str, markeveryParameter=1):
    plt.rcParams.update({'font.size': 16})
    plt.rcParams["font.family"] = "sans-serif"
    fig = plt.figure(figsize=(14, 12))
    ax = fig.add_subplot(1,1,1)

    ax.plot(nObjFuncEval, functionList, 'r', linestyle="-.", linewidth=3, markersize=8, marker="o", markevery=markeveryParameter, label=name)
    ax.legend(fontsize=20)
    ax.grid()
    ax.set_xlabel('Number of Function Evaluations', fontsize=20, labelpad=13)
    ax.set_ylabel('Function Value', fontsize=20, labelpad=15)
    plt.title(name)
    fig.savefig(f'funcByNEval_{name}.png', dpi=fig.dpi)

    
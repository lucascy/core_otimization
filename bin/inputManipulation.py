'''
Author: Lucas Cytrangulo
Date: 15/05/2023
'''

def readInput(inputPath: str):
    '''Read input for Coppe-UFRJ files

    Args:\n
        inputPath (str): path to the input file with the variables\n

    Returns:\n
        variables (list): list with the variables\n   
    '''
    varFile =  open(inputPath, "r")
    tmp = varFile.readlines()
    variables = [float(i) for i in tmp]
    varFile.close()
    return variables

def writeNewInput(inputPath: str, variables: list):
    '''Write input for Coppe-UFRJ files

    Args:\n
        inputPath (str): path to the input file with the variables\n

    Returns:\n
        variables (list): list with the variables\n   
    '''
    varFile = open(inputPath, "w")
    tmp = [str(i)+'\n' for i in variables]
    varFile.writelines(tmp)
    varFile.close()
    
def readOutput(inputPath: str):
    '''Read output for Coppe-UFRJ files

    Args:\n
        inputPath (str): path to the input file with the variables\n

    Returns:\n
        variables (list): list with the variables\n   
    '''
    objFile =  open(inputPath, "r")
    tmp = objFile.read()
    variable = float(tmp)  
    objFile.close()
    return variable


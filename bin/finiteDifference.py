"""
This module calculates the finite differrence derivative using 3 approximations.


Author: Lucas Cytrangulo
Date: 13/05/2023
"""

small = 1e-323 

def forward(U: float, U_F: float, delta: float):
    '''Calculates forward difference first derivative

    Args:
        U (float): function value at actual point
        U_F (float): value of the function at the current point plus a perturbation (U Forward)
        delta (float): value of the pertubation        
    Returns:
        dU (float): derivative value 
    '''
    dU = (U_F - U) / (delta + small)
    return dU


def backward(U: float, U_B: float, delta: float):
    '''Calculates backward difference first derivative

    Args:
        U (float): function value at actual point
        U_B(float): value of the function at the current point minus a perturbation (U Backward)
        delta (float): value of the pertubation        
    Returns:
        dU (float): derivative value 
    '''
    dU = (U - U_B) / (delta + small)
    return dU

def central(U_B: float, U_F: float, delta: float):
    '''Calculates central difference first derivative

    Args:
        U_B (float): value of the function at the current point minus a perturbation (U Backward)
        U_F (float): value of the function at the current point plus a perturbation (U Forward)
        delta (float): value of the pertubation        
    Returns:
        dU (float): derivative value 
    '''
    dU = (U_F - U_B) / ((2 * delta) + small)
    return dU

def second_forward(U_F1: float, U_F2: float, U: float, delta: float):
    '''Calculates forward difference second derivative

    Args:
        U (float): function value at actual point
        U_F1 (float): value of the function at the current point plus a perturbation (U Forward 1)
        U_F2 (float): value of the function at the current point plus two perturbations (U Forward 2)
        delta (float): value of the pertubation        
    Returns:
        dU2 (float): second derivative value 
    '''
    dU2 = (U_F2 - 2*U_F1 + U)/(delta**2 + small)
    return dU2

def second_backward(U_B1: float, U_B2: float, U: float, delta: float):
    '''Calculates backward difference second derivative

    Args:
        U (float): function value at actual point
        U_B1 (float): value of the function at the current point minus a perturbation (U Backward 1)
        U_B2 (float): value of the function at the current point minus two perturbations (U Backward 2)
        delta (float): value of the pertubation        
    Returns:
        dU2 (float): second derivative value 
    '''
    dU2 = (U - 2*U_B1 + U_B2)/(delta**2 + small)
    return dU2

def second_central(U_F1: float, U_B1: float, U: float, delta: float):
    '''Calculates centered difference second derivative

    Args:
        U (float): function value at actual point
        U_B1 (float): value of the function at the current point minus a perturbation (U Backward 1)
        U_F1 (float): value of the function at the current point plus a perturbation (U Forward 1)
        delta (float): value of the pertubation        
    Returns:
        dU2 (float): second derivative value 
    '''
    dU2 = (U_F1 - 2*U + U_B1)/(delta**2 + small)
    return dU2

def cross_second(U_FF: float, U_FB: float, U_BF:float, U_BB: float, delta1: float, delta2:float):
    '''Calculates centered difference second cross derivative

    Args:
        U_FF (float): value of the function at the current point plus a perturbation in both variables 
        U_FB (float): value of the function at the current point plus a pertubation on the first variable
        minus a perturbation on the second one
        U_BF (float): value of the function at the current point minus a pertubation on the first variable
        plus a perturbation on the second one
        U_BB (float): value of the function at the current point minus a perturbation in both variables
        delta1 (float): value of the pertubation on the first variable
        delta2 (float): value of the pertubation on the second variable    
    Returns:
        dU2 (float): second cross derivative value 
    '''
    dUcross = (U_FF - U_FB - U_BF + U_BB)/((4*delta1*delta2)+small)
    return dUcross
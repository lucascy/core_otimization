import subprocess
import os
import shutil
import sys
sys.path.append('../')
import etc.envVar as EV

"""
This module receives the parameters, set the input file to the objective 
function, run the objective function and return its value.

It has two methods: 1. run on SRC dir, or run locally (wherever it's called) 

Author: Lucas Cytrangulo
Date: 07/05/2023
"""

def origDir(inputPath: str):#(ID: int, number_of_variables: int, valuesList: list):
    '''Run objective function at the project path

    Args:\n
        inputPath (str): path to the input file\n
        
    Returns:\n
        U (float): Funtion value at the calculated variables values\n
    '''
    #SRCpath = os.environ['OTIMIZATION_CORE_SRC']
    SRCpath = EV.CORE_SRC
    #varFile = open(f"{SRCpath}/fucntionsModule/var.dat","w")
    #varFile.write(f"{ID}\n")
    #varFile.write(f"{number_of_variables}\n")
    #i = 0
    #for i in range(number_of_variables):
    #    varFile.write(str(valuesList[i])+"\n")
    #    i += 1
    #varFile.close()
    modulePath = f"{SRCpath}/fucntionsModule/var.dat"
    shutil.copyfile(inputPath, modulePath)
    pathRUN = f"{SRCpath}/fucntionsModule/run.sh"
    subprocess.call(['sh', pathRUN]) 
    objFile =  open(f"{SRCpath}/fucntionsModule/obj.dat", "r")
    U = objFile.read()
    objFile.close()
    return float(U)

def runDir(inputPath: str):#(ID: int, number_of_variables: int, valuesList: list):
    '''Run objective function at the project path

    Args:\n
        inputPath (str): path to the input file\n
        
    Returns:\n
        U (float): Funtion value at the calculated variables values\n
    '''
    runDir = os.getcwd()
    #SRCpath = os.environ['OTIMIZATION_CORE_SRC']
    SRCpath = EV.CORE_SRC
    module = SRCpath + "/functionsModule"
    shutil.copytree(module, runDir+"/functionsModule", dirs_exist_ok=True)
    #varFile = open(f"{runDir}/functionsModule/var.dat", "w")
    #varFile.write(f"{ID}\n")
    #varFile.write(f"{number_of_variables}\n")
    #i = 0
    #for i in range(number_of_variables):
    #    varFile.write(str(valuesList[i])+"\n")
    #    i += 1
    #varFile.close()
    moduleLocalPath = f"{runDir}/functionsModule/var.dat"
    shutil.copyfile(inputPath, moduleLocalPath)
    pathRUN = f"{runDir}/functionsModule/run.sh"
    subprocess.call(['sh', pathRUN]) 
    objFile =  open(f"{runDir}/functionsModule/obj.dat", "r")
    U = objFile.read()
    objFile.close()
    return float(U)
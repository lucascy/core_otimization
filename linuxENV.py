import os

core = os.getcwd()
OTIMIZATION_CORE_BIN = f"{core}/bin"
OTIMIZATION_CORE_SRC = f"{core}/src"
OTIMIZATION_CORE_ETC = f"{core}/etc"

bashrc = open(f"{OTIMIZATION_CORE_ETC}/bashrc","w")
bashrc.writelines(f"""#----------------------------------*-sh-*--------------------------------------
# File
#     etc/bashrc
#
# Description
#     The file contain definition of variables needed 
#     to configure a otimization package enviroment.
#
#------------------------------------------------------------------------------
DIR="$( cd "$( dirname "${{BASH_SOURCE[0]}}" )" >/dev/null && pwd )" 
DIR=${{DIR%/*}} 
#------------------------------------------------------------------------------

# Location of the project installation 
export OTIMIZATION_CORE_DIR="{core}"
export OTIMIZATION_CORE_BIN="{OTIMIZATION_CORE_BIN}"
export OTIMIZATION_CORE_SRC="{OTIMIZATION_CORE_SRC}"
export OTIMIZATION_CORE_ETC="{OTIMIZATION_CORE_ETC}"
""")
bashrc.close()

envEdit = os.path.expanduser('~')
envEdit = envEdit + "/.bashrc"

fp = open(envEdit,"a")
fp.write(f"source {OTIMIZATION_CORE_ETC}/bashrc")
fp.close()
import os

CORE = os.getcwd()
CORE_BIN = f"{CORE}/bin"
CORE_SRC = f"{CORE}/src"
CORE_ETC = f"{CORE}/etc"

envVarList = [f"CORE='{CORE}'\n", f"CORE_BIN='{CORE_BIN}'\n", f"CORE_SRC='{CORE_SRC}'\n", f"CORE_ETC='{CORE_ETC}'\n"]

envFile =  open("./etc/envVar.py", "w")
tmp = envFile.writelines(envVarList)
envFile.close()
import sys
sys.path.append('../')
import bin.inputManipulation as im
import bin.gradients as grads
import bin.objectiveFunction as of
import numpy as np
import os
from scipy.optimize import minimize
from functools import partial

'''
Optimization functions - deterministics methods

Author: Lucas Cytrangulo
Date: 13/05/2023
'''

def steepestDescent(alpha: float, inputPath: str, gradMetod="forward", delta=1e-8, maxIter=1000, epsilon=1e-8, verbose=True):
    """Steepest Descent Method
    Otimitization algorithm, using steepest descent method, with user determined finite difference gradient approximation method, and 
    convergence ruled by the gradient norm.

    Args:\n
        alpha (float): method step value\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        gradMethod (str): finite difference gradient approximation method (should be either "central", "formard" or "backward)\n
        delta (float): pertubation to calculate the gradient by finite difference approximation\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the gradient norm for the convergence of the method\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    varList = im.readInput(inputPath)
    gradMetods = ["central", "forward", "backward"]
    if gradMetod not in gradMetods:
        raise ValueError("Invalid gradient method. Expected one of: %s" % gradMetods)
    gradMetods = {"central": grads.grad_central, "forward": grads.grad_forward, "backward": grads.grad_backward}
    fEvalGrad = {"central": (varList[1]*2), "forward": 1+varList[1], "backward": 1+varList[1]}
    chosen_gradMethod = gradMetods.get(gradMetod)
    chosen_fEvalGrad = fEvalGrad.get(gradMetod)

    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    varListIter = [e for e in varList]
    iteration = 0
    functionList = []
    nObjFuncEval = [0] 
    varListIterPrint = [varListIter[2:]]
    gradNorm = 1e30
    while iteration < maxIter and gradNorm > epsilon:
        iteration+=1
        gradList = chosen_gradMethod(inputPath, delta)
        #print(gradList)
        gradArray = np.array(gradList)
        gradNorm = np.dot(gradArray,gradArray)
        i = 0
        functionList.append(im.readOutput(objPath))
        nObjFuncEval.append(chosen_fEvalGrad * iteration) 
        while i < varList[1]:
            x = varListIter[i+2] - alpha*gradList[i]
            varListIter[i+2] = x
            i += 1
        im.writeNewInput(inputPath, varListIter)
        varListIterPrint.append(varListIter[2:])
        if verbose == True:  
            print(f"ITERATION {iteration}:\n")
            print("Initial function Value:")
            print(f"{functionList[-1]:.8e}")
            print("Final calculated variables:")
            print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
            print("__________________________________________________\n")
        else:
            pass
    if gradNorm < epsilon:
        of.runDir(inputPath)
        functionList.append(im.readOutput(objPath))
        print(f"Otimization converged in {iteration} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    return varListIterPrint, functionList, nObjFuncEval
        



def user_conjugateGradient(alpha: float, inputPath: str, gradMetod="forward", delta=1e-8, maxIter=1000, epsilon=1e-8, verbose=True):
    """Conjugate Gradient Method
    Otimitization algorithm, using conjugate gradient method, with user determined finite difference gradient approximation method, and 
    convergence ruled by the gradient norm.

    Args:\n
        alpha (float): method step value\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        gradMethod (str): finite difference gradient approximation method (should be either "central", "formard" or "backward)\n
        delta (float): pertubation to calculate the gradient by finite difference approximation\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the gradient norm for the convergence of the method\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    varList = im.readInput(inputPath)
    gradMetods = ["central", "forward", "backward"]
    if gradMetod not in gradMetods:
        raise ValueError("Invalid gradient method. Expected one of: %s" % gradMetods)
    gradMetods = {"central": grads.grad_central, "forward": grads.grad_forward, "backward": grads.grad_backward}
    fEvalGrad = {"central": (varList[1]*2), "forward": 1+varList[1], "backward": 1+varList[1]}
    chosen_gradMethod = gradMetods.get(gradMetod)
    chosen_fEvalGrad = fEvalGrad.get(gradMetod)

    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    varListIter = [e for e in varList]
    iteration = 0
    functionList = []
    nObjFuncEval = [0] 
    varListIterPrint = [varListIter[2:]]

    #first iteration
    iteration+=1
    gradList = chosen_gradMethod(inputPath, delta)
    gradArray = np.array(gradList)
    print(gradArray)
    gradNorm = np.dot(gradArray,gradArray)
    i = 0
    functionList.append(im.readOutput(objPath))
    nObjFuncEval.append(chosen_fEvalGrad * iteration)
    while i < varList[1]:
        x = varListIter[i+2] - alpha*gradList[i]
        varListIter[i+2] = x
        i += 1
    im.writeNewInput(inputPath, varListIter)
    varListIterPrint.append(varListIter[2:])
    if verbose == True:  
        print(f"ITERATION {iteration}:\n")
        print("Initial function Value:")
        print(f"{functionList[-1]:.8e}")
        print("Final calculated variables:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
        print("__________________________________________________\n")
    else:
        pass
    #rest of the iterations
    while iteration < maxIter and gradNorm > epsilon:
        iteration+=1
        gradOld = gradArray
        gradList = chosen_gradMethod(inputPath, delta)
        gradArray = np.array(gradList)
        gradNorm_old = gradNorm
        gradNorm = np.dot(gradArray,gradArray)
        gamma = gradNorm/gradNorm_old
        d = -gradArray + gamma*(-gradOld)
        i = 0
        functionList.append(im.readOutput(objPath))
        nObjFuncEval.append(chosen_fEvalGrad * iteration)
        while i < varList[1]:
            x = varListIter[i+2] + alpha*d[i]
            varListIter[i+2] = x
            i+=1
        im.writeNewInput(inputPath, varListIter)
        varListIterPrint.append(varListIter[2:])
        if verbose == True:  
            print(f"ITERATION {iteration}:\n")
            print("Initial function Value:")
            print(f"{functionList[-1]:.8e}")
            print("Final calculated variables:")
            print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
            print("__________________________________________________\n")
        else:
            pass
    if gradNorm < epsilon:
        of.runDir(inputPath)
        functionList.append(im.readOutput(objPath))
        print(f"Otimization converged in {iteration} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    return varListIterPrint, functionList, nObjFuncEval

'''_____________________________________________###_________________________________________________________'''
# objective function for scipy methods
def objective(x, inputPath, delta, gradMetod="forward"):
    varList = im.readInput(inputPath)
    varListIter = [e for e in varList]
    n = varListIter[1]
    i = 0
    while i < n:
        varListIter[i+2] = x[i]
        i += 1
    im.writeNewInput(inputPath, varListIter)
    x = of.runDir(inputPath)
    return x 

# derivative of the objective function for scipy methods
def derivative(x, inputPath, delta, gradMetod="forward"):
    gradMetods = ["central", "forward", "backward"]
    if gradMetod not in gradMetods:
        raise ValueError("Invalid gradient method. Expected one of: %s" % gradMetods)
    gradMetods = {"central": grads.grad_central, "forward": grads.grad_forward, "backward": grads.grad_backward}
    chosen_gradMethod = gradMetods.get(gradMetod)

    varList = im.readInput(inputPath)
    varListIter = [e for e in varList]
    n = varListIter[1]
    i = 0
    while i < n:
        varListIter[i+2] = x[i]
        i += 1
    im.writeNewInput(inputPath, varListIter)
    gradList = chosen_gradMethod(inputPath, delta)
    gradArray = np.array(gradList)
    return gradArray

#callback function
Nfeval = 1
functionList = []
def callbackF(Xi, verbose: bool):
    global Nfeval
    global functionList
    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    functionList.append(im.readOutput(objPath))
    if verbose == True:
        print(f"ITERATION {Nfeval}:\n")
        print("Function Value:")
        print(f"{functionList[-1]:.8e}")
        print("Final calculated variables:")
        print('\t'.join(f'{var:.8e}' for var in Xi))
        print("__________________________________________________\n")
    Nfeval += 1
'''_____________________________________________###_________________________________________________________'''


def conjugateGradient(alpha: float, inputPath: str, gradMetod="forward", delta=1e-8, maxIter=1000, epsilon=1e-8, verbose=True):
    """Conjugate Gradient Method
    Otimitization algorithm, using conjugate gradient method, with user determined finite difference gradient approximation method, and 
    convergence ruled by the gradient norm.

    Args:\n
        alpha (float): method step value\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        gradMethod (str): finite difference gradient approximation method (should be either "central", "formard" or "backward)\n
        delta (float): pertubation to calculate the gradient by finite difference approximation\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the gradient norm for the convergence of the method\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    #currentDir = os.getcwd()
    #objPath = currentDir  + "/functionsModule/obj.dat"
    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    of.runDir(inputPath)
    functionList.append(im.readOutput(objPath))
    pt = im.readInput(inputPath)[2:]
    scipyOpt = minimize(objective, pt, method='CG', callback=partial(callbackF, verbose = verbose),args=(inputPath, delta, gradMetod), jac=derivative, options={'maxiter':maxIter, 'gtol':epsilon, 'return_all':True, 'eps':alpha})
    varListIter = scipyOpt['allvecs']
    if scipyOpt['message'] == 'Optimization terminated successfully.':
        print(f"Otimization converged in {Nfeval-1} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[-1]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    else:
        print(scipyOpt['message'])
    nVar = im.readInput(inputPath)[1]
    if gradMetod == 'forward' or gradMetod == 'backward':
        final_nObjFuncEval = (scipyOpt['nfev'] + scipyOpt['njev'] * (1+nVar)) + 1
    elif gradMetod == 'central':
        final_nObjFuncEval = (scipyOpt['nfev'] + scipyOpt['njev'] * (2*nVar)) + 1
    nObjFuncEval = np.linspace(0, final_nObjFuncEval-1, int(Nfeval))#fake
    functionList2 = [e for e in functionList]
    functionList.clear()
    return varListIter, functionList2, nObjFuncEval


def user_newton(inputPath: str, gradMetod="forward", delta=1e-8, maxIter=1000, epsilon=1e-8, verbose=True):
    """Newton's Method
    Otimitization algorithm, using newton's method, with user determined finite difference gradient approximation method, and 
    convergence ruled by the gradient norm.

    Args:\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        gradMethod (str): finite difference gradient approximation method (should be either "central", "formard" or "backward)\n
        delta (float): pertubation to calculate the gradient by finite difference approximation\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the gradient norm for the convergence of the method\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    varList = im.readInput(inputPath)
    gradMetods = ["central", "forward", "backward"]
    if gradMetod not in gradMetods:
        raise ValueError("Invalid gradient method. Expected one of: %s" % gradMetods)
    gradMetods = {"central": grads.grad_central, "forward": grads.grad_forward, "backward": grads.grad_backward}
    fEvalGrad = {"central": (varList[1]*2), "forward": 1+varList[1], "backward": 1+varList[1]}
    chosen_gradMethod = gradMetods.get(gradMetod)
    chosen_fEvalGrad = fEvalGrad.get(gradMetod)

    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    varList = im.readInput(inputPath)
    varListIter = [e for e in varList]
    x = np.array(varListIter[2:])
    iteration = 0
    functionList = []
    nObjFuncEval = [0] 
    varListIterPrint = [varListIter[2:]]
    gradNorm = 1e30
    while iteration < maxIter and gradNorm > epsilon:
        iteration+=1
        gradList = chosen_gradMethod(inputPath, delta)
        gradArray = np.array(gradList)
        gradNorm = np.dot(gradArray,gradArray)
        Hvec = np.array(grads.second_derivative_central(inputPath, delta))
        #Hvec += delta
        #print(f'Hvec: {Hvec}')
        Hessian_diag = np.diag(Hvec)
        Hessian_cross = grads.second_cross_derivative(inputPath, delta)
        #anti_xabu = np.ones(Hessian_diag.shape)*1e-64
        #anti_xabu[0][0] = anti_xabu[0][0]*1e-64
        Hessian = Hessian_diag + Hessian_cross #+ anti_xabu

        #print(f'Hessian: {Hessian}')
        #print(np.linalg.det(Hessian))
        invHes = np.linalg.inv(Hessian)
        x = x - np.matmul(invHes,gradArray)

        i = 0
        functionList.append(im.readOutput(objPath))
        nObjFuncEval.append(chosen_fEvalGrad * iteration) 
        while i < varList[1]:
            varListIter[i+2] = x[i]
            i += 1
        im.writeNewInput(inputPath, varListIter)
        varListIterPrint.append(varListIter[2:])
        if verbose == True:  
            print(f"ITERATION {iteration}:\n")
            print("Initial function Value:")
            print(f"{functionList[-1]:.8e}")
            print("Final calculated variables:")
            print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
            print("__________________________________________________\n")
        else:
            pass
    if gradNorm < epsilon:
        of.runDir(inputPath)
        functionList.append(im.readOutput(objPath))
        print(f"Otimization converged in {iteration} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    return varListIterPrint, functionList, nObjFuncEval

def newton(inputPath: str, gradMetod="forward", delta=1e-8, maxIter=1000, epsilon=1e-8, verbose=True):
    """Newtons Method
    Otimitization algorithm, using newtons method, with user determined finite difference gradient approximation method, and 
    convergence ruled by the gradient norm.

    Args:\n
        alpha (float): method step value\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        gradMethod (str): finite difference gradient approximation method (should be either "central", "formard" or "backward)\n
        delta (float): pertubation to calculate the gradient by finite difference approximation\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the gradient norm for the convergence of the method\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    #currentDir = os.getcwd()
    #objPath = currentDir  + "/functionsModule/obj.dat"
    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    of.runDir(inputPath)
    functionList.append(im.readOutput(objPath))
    pt = im.readInput(inputPath)[2:]
    scipyOpt = minimize(objective, pt, method='Newton-CG', callback=partial(callbackF, verbose = verbose),args=(inputPath, delta, gradMetod), jac=derivative, options={'maxiter':maxIter, 'gtol':epsilon, 'return_all':True})
    varListIter = scipyOpt['allvecs']
    if scipyOpt['message'] == 'Optimization terminated successfully.':
        print(f"Otimization converged in {Nfeval-1} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[-1]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    else:
        print(scipyOpt['message'])
    nVar = im.readInput(inputPath)[1]
    if gradMetod == 'forward' or gradMetod == 'backward':
        final_nObjFuncEval = (scipyOpt['nfev'] + scipyOpt['njev'] * (1+nVar)) + 1
    elif gradMetod == 'central':
        final_nObjFuncEval = (scipyOpt['nfev'] + scipyOpt['njev'] * (2*nVar)) + 1
    nObjFuncEval = np.linspace(0, final_nObjFuncEval-1, int(Nfeval))#fake
    functionList2 = [e for e in functionList]
    functionList.clear()
    return varListIter, functionList2, nObjFuncEval

def user_BFGS(alpha: float, inputPath: str, gradMetod="forward", delta=1e-8, maxIter=1000, epsilon=1e-8, verbose=True):
    """BFGS optimization method
    Otimitization algorithm, using BFGS method, with user determined finite difference gradient approximation method, and 
    convergence ruled by the gradient norm.

    Args:\n
        alpha (float): method step value\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        gradMethod (str): finite difference gradient approximation method (should be either "central", "formard" or "backward)\n
        delta (float): pertubation to calculate the gradient by finite difference approximation\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the gradient norm for the convergence of the method\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    varList = im.readInput(inputPath)
    gradMetods = ["central", "forward", "backward"]
    if gradMetod not in gradMetods:
        raise ValueError("Invalid gradient method. Expected one of: %s" % gradMetods)
    gradMetods = {"central": grads.grad_central, "forward": grads.grad_forward, "backward": grads.grad_backward}
    fEvalGrad = {"central": (varList[1]*2), "forward": 1+varList[1], "backward": 1+varList[1]}
    chosen_gradMethod = gradMetods.get(gradMetod)
    chosen_fEvalGrad = fEvalGrad.get(gradMetod)

    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    varList = im.readInput(inputPath)
    varListIter = [e for e in varList]
    n = int(varListIter[1])
    iteration = 0
    functionList = []
    nObjFuncEval = [0]
    varListIterPrint = [varListIter[2:]]
    #initializatiom of the variables
    gradNorm = 1e30
    gradArray = np.empty(n)
    H = np.identity(n)
    x = np.array(varListIter[2:])
    while iteration < maxIter and gradNorm > epsilon:
        iteration+=1
        gradOld = gradArray
        gradList = chosen_gradMethod(inputPath, delta)
        gradArray = np.array(gradList)
        gradNorm = np.dot(gradArray,gradArray)
        if iteration == 1:
            H = H
        else:
            Y = gradArray - gradOld
            M = (1. + np.dot(Y,np.matmul(H,Y)))/np.dot(Y,d) * np.outer(d,d)/np.dot(d,Y)
            N = - (np.matmul(np.outer(d,Y), H) + np.outer(np.matmul(H,Y),d)) / np.dot(Y, d)
            H=H+M+N
        d = - np.matmul(H, gradArray)
        x = x + alpha*d
        i = 0
        functionList.append(im.readOutput(objPath))
        nObjFuncEval.append(chosen_fEvalGrad * iteration) 
        while i < n:
            varListIter[i+2] = x[i]
            i += 1
        im.writeNewInput(inputPath, varListIter)
        varListIterPrint.append(varListIter[2:])
        if verbose == True:  
            print(f"ITERATION {iteration}:\n")
            print("Initial function Value:")
            print(f"{functionList[-1]:.8e}")
            print("Final calculated variables:")
            print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
            print("__________________________________________________\n")
        else:
            pass
    if gradNorm < epsilon:
        of.runDir(inputPath)
        functionList.append(im.readOutput(objPath))
        print(f"Otimization converged in {iteration} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[2:]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    return varListIterPrint, functionList, nObjFuncEval


def BFGS(alpha: float, inputPath: str, gradMetod="forward", delta=1e-8, maxIter=1000, epsilon=1e-8, verbose=True):
    """BFGS Method
    Otimitization algorithm, using conjugate gradient method, with user determined finite difference gradient approximation method, and 
    convergence ruled by the gradient norm.

    Args:\n
        alpha (float): method step value\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        gradMethod (str): finite difference gradient approximation method (should be either "central", "formard" or "backward)\n
        delta (float): pertubation to calculate the gradient by finite difference approximation\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the gradient norm for the convergence of the method\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    #currentDir = os.getcwd()
    #objPath = currentDir  + "/functionsModule/obj.dat"
    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    of.runDir(inputPath)
    functionList.append(im.readOutput(objPath))
    pt = im.readInput(inputPath)[2:]
    scipyOpt = minimize(objective, pt, method='BFGS', callback=partial(callbackF, verbose = verbose),args=(inputPath, delta, gradMetod), jac=derivative, options={'maxiter':maxIter, 'gtol':epsilon, 'return_all':True, 'eps':alpha})
    varListIter = scipyOpt['allvecs']
    if scipyOpt['message'] == 'Optimization terminated successfully.':
        print(f"Otimization converged in {Nfeval-1} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[-1]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    else:
        print(scipyOpt['message'])
    nVar = im.readInput(inputPath)[1]
    if gradMetod == 'forward' or gradMetod == 'backward':
        final_nObjFuncEval = (scipyOpt['nfev'] + scipyOpt['njev'] * (1+nVar)) + 1
    elif gradMetod == 'central':
        final_nObjFuncEval = (scipyOpt['nfev'] + scipyOpt['njev'] * (2*nVar)) + 1
    nObjFuncEval = np.linspace(0, final_nObjFuncEval-1, int(Nfeval))#fake
    functionList2 = [e for e in functionList]
    functionList.clear()
    return varListIter, functionList2, nObjFuncEval
import sys
sys.path.append('../')
import bin.inputManipulation as im
import bin.gradients as grads
import bin.objectiveFunction as of
import numpy as np
import os
from scipy.optimize import differential_evolution, dual_annealing
from functools import partial
import random
'''
Optimization functions - heuristics methods

Author: Lucas Cytrangulo
Date: 14/07/2023
'''

# objective function for scipy methods
def objective(x, inputPath):
    varList = im.readInput(inputPath)
    varListIter = [e for e in varList]
    n = varListIter[1]
    i = 0
    while i < n:
        varListIter[i+2] = x[i]
        i += 1
    im.writeNewInput(inputPath, varListIter)
    x = of.runDir(inputPath)
    return x 

#callback function
Nfeval = 1
functionList = []
varListIter = []
def callbackF(Xi, convergence, verbose: bool):
    global Nfeval
    global functionList
    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    functionList.append(im.readOutput(objPath))
    varListIter.append(Xi)
    if verbose == True:
        print(f"ITERATION {Nfeval}:\n")
        print("Function Value:")
        print(f"{functionList[-1]:.8e}")
        print("Best value of the calculated variables:")
        print('\t'.join(f'{var:.8e}' for var in Xi))
        print("__________________________________________________\n")
    Nfeval += 1

def callbackF2(Xi,  f, context, epsilon: float, verbose: bool):
    global Nfeval
    global functionList
    currentDir = os.getcwd()
    objPath = currentDir  + "/functionsModule/obj.dat"
    functionList.append(im.readOutput(objPath))
    varListIter.append(Xi)
    if verbose == True:
        print(f"ITERATION {Nfeval}:\n")
        print("Function Value:")
        print(f"{functionList[-1]:.8e}")
        print("Best value of the calculated variables:")
        print('\t'.join(f'{var:.8e}' for var in Xi))
        print("__________________________________________________\n")
    Nfeval += 1
    if Nfeval > 2:
        old = functionList[-2]
        actual = functionList[-1]
        discrepancy = np.abs((old - actual)/actual)
        if discrepancy < epsilon:
            return True
    # if context == 0:
    #     return True

'''_____________________________________________###_________________________________________________________'''


def differentialEvolution(bounds: list, inputPath: str, n_pop=15, maxIter=1000, epsilon=1e-8, tx_mutation=(0,0.5), CR=0.7, verbose=True):
    """Differential Evolution Method
    Otimitization algorithm, using differential evolution method, with convergence ruled by a relative tolerance.

    Args:\n
        bounds (list): list of the bounds of the variables - ex: bounds = [(x1_min x1_max),(x2_min x2_max),...]\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        n_pop (int): number of individual for the population\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the relative tolerance for the convergence of the method -> "np.std(pop) <= atol + tol * 
        np.abs(np.mean(population_energies))"\n
        tx_mutation (tuple): mutation tax interval\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    scipyOpt = differential_evolution(objective, 
                                      bounds=bounds,
                                      args=(inputPath,),
                                      strategy='best1bin',
                                      maxiter=maxIter,
                                      popsize=n_pop,
                                      tol=epsilon,
                                      mutation=tx_mutation,
                                      recombination=CR,
                                      callback=partial(callbackF, verbose = verbose),
                                      polish=False)
    if scipyOpt['message'] == 'Optimization terminated successfully.':
        print(f"Otimization converged in {Nfeval-1} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[-1]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    else:
        print(scipyOpt['message'])
    final_nObjFuncEval = scipyOpt['nfev']
    nObjFuncEval = np.linspace(0, final_nObjFuncEval-1, int(Nfeval-1))#fake
    varListIter2 = [e for e in varListIter]
    varListIter.clear()
    functionList2 = [e for e in functionList]
    functionList.clear()
    return varListIter2, functionList2, nObjFuncEval

def particleSwarm(bounds: list, inputPath: str, n_pop=15, maxIter=1000, epsilon=1e-8, verbose=True):
    '''Particle Swarm Method
    Otimitization algorithm, using particle swarm method, with convergence ruled by a relative tolerance.

    Args:\n
        bounds (list): list of the bounds of the variables - ex: bounds = [(x1_min x1_max),(x2_min x2_max),...]\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        n_pop (int): number of individual for the population\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the relative tolerance for the convergence of the method -> "np.std(pop) <= atol + tol * 
        np.abs(np.mean(population_energies))"\n
        tx_mutation (tuple): mutation tax interval\n
        verbose (bool): switch to print or do not print the convergence\n

    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    '''
    #position_min = bounds[0]
    #position_max = bounds[1]
    varList = im.readInput(inputPath)
    varListIter = []
    n = int(varList[1])
    #particles = [[random.uniform(position_min[0], position_max[1]) for j in range(n)] for i in range(n_pop)]
    particles = np.zeros((n_pop,n), dtype=np.float64)
    i = 0
    while i in range(n_pop):
        j = 0
        while j in range(n):
            r = np.random.rand()
            position_min = bounds[j][0]
            position_max = bounds[j][-1]
            sub = position_min + r*(position_max - position_min)
            particles[i][j] = sub
            j += 1
        i+=1

    pbest_position = particles
    pbest_function = []
    for p in particles:
        pbest_function.append(objective(p,inputPath))

    gbest_index = np.argmin(pbest_function)
    gbest_function = pbest_function[gbest_index]
    gbest_position = pbest_position[gbest_index]
    
    velocity = [[0.0 for j in range(n)] for i in range(n_pop)]
    error_0 = (np.abs(np.average(pbest_function) - gbest_function))/gbest_function
    nObjFuncEval = []
    for t in range(maxIter):
        error = (np.abs(np.average(pbest_function) - gbest_function))/gbest_function
        if np.abs(1-error) <= epsilon:
            print(f"Otimization converged in {t} iterations.")
            print("Final Variables' Values:")
            print('\t'.join(f'{var:.8e}' for var in varListIter[-1]))
            print("Function's Value:")
            print(f"{functionList[-1]:.8e}")
            break
        else:
            r1 = np.random.rand()
            r2 = np.random.rand()
            beta = 2
            a = 1/((np.abs(1 - error_0)) - 2*epsilon)
            b = -(2*epsilon)/((np.abs(1 - error_0)) - 2*epsilon)
            alpha = a*(np.abs(1-error)) - b #linear decay of alpha (1 in the begining and 0 at the 200% of desired error)
            if t > 0.7*maxIter:
                alpha = 0
            #alpha = 0.1
            #print(alpha)

            for m in range(n_pop):
                for i in range(len(particles[m])): 
                    velocity[m][i] = alpha*velocity[m][i] + beta*r1*(pbest_position[m][i]-particles[m][i])+beta*r2*(gbest_position[i]-particles[m][i])
                    
                particles[m] += velocity[m]
        pbest_function = []
        for p in particles:
            pbest_function.append(objective(p,inputPath))

        gbest_index = np.argmin(pbest_function)
        gbest_position = pbest_position[gbest_index]

        varListIter.append(gbest_position)
        functionList.append(min(pbest_function))
        nObjFuncEval.append(n_pop*(t+1))

        if verbose == True:
            # print('Global Best Position: ', gbest_position)
            # print('Best Fitness Value: ', min(pbest_function))
            # print('Average Particle Best Fitness Value: ', np.average(pbest_function))
            # print('Number of Generation: ', t+1)
            print(f"ITERATION {t+1}:\n")
            print("Function Value:")
            print(f"{functionList[-1]:.8e}")
            print("Best value of the calculated variables:")
            print('\t'.join(f'{var:.8e}' for var in gbest_position))
            print("__________________________________________________\n")
        else:
            pass
    
    varListIter2 = [e for e in varListIter]
    varListIter.clear()
    functionList2 = [e for e in functionList]
    functionList.clear()
    return varListIter2, functionList2, nObjFuncEval

def simulatedAnnealing(bounds: list, inputPath: str, T=6000, TR=1e-5, epsilon = 1e-5, maxIter=1000, verbose=True):
    """Simulated Annealing Method
    Otimitization algorithm, using simulated annealing method, with convergence ruled by a relative tolerance.

    Args:\n
        bounds (list): list of the bounds of the variables - ex: bounds = [(x1_min x1_max),(x2_min x2_max),...]\n
        inputPath (str): input file path for the objective function (this file is modified during run time)\n
        
        n_pop (int): number of individual for the population\n
        maxIter (int): maximum number of iterations\n
        epsilon (float): target value for the relative tolerance for the convergence of the method -> "np.std(pop) <= atol + tol * 
        np.abs(np.mean(population_energies))"\n
        tx_mutation (tuple): mutation tax interval\n
        verbose (bool): switch to print or do not print the convergence\n
        
    Returns:\n
        varListIter (list): List with function's parameters at the last iteration\n
        functionList (list): List with function's values along the iterations\n
    """
    scipyOpt = dual_annealing(objective,
                              bounds=bounds,
                              args=(inputPath,),
                              maxiter=maxIter,
                              initial_temp=T,
                              restart_temp_ratio=TR,
                              callback=partial(callbackF2, epsilon = epsilon, verbose = verbose),
                              no_local_search=True)
    if scipyOpt['message'] == ['Callback function requested to stop early by returning True']:#rataria
        print(f"Otimization converged by the tolerance of {epsilon} in {Nfeval-1} iterations.")
        print("Final Variables' Values:")
        print('\t'.join(f'{var:.8e}' for var in varListIter[-1]))
        print("Function's Value:")
        print(f"{functionList[-1]:.8e}")
    else:
        print(scipyOpt['message'])
    final_nObjFuncEval = scipyOpt['nfev']
    nObjFuncEval = np.linspace(0, final_nObjFuncEval-1, int(Nfeval-1))#fake
    varListIter2 = [e for e in varListIter]
    varListIter.clear()
    functionList2 = [e for e in functionList]
    functionList.clear()
    return varListIter2, functionList2, nObjFuncEval